# A Laravel package to plug Headless CMS like Strapi, Wordpress etc. in your Laravel App

[![Latest Version on Packagist](https://img.shields.io/packagist/v/blok/laravel-cms.svg?style=flat-square)](https://packagist.org/packages/blok/laravel-cms)
[![GitHub Tests Action Status](https://img.shields.io/github/workflow/status/blok/laravel-cms/run-tests?label=tests)](https://github.com/blok/laravel-cms/actions?query=workflow%3Arun-tests+branch%3Amain)
[![GitHub Code Style Action Status](https://img.shields.io/github/workflow/status/blok/laravel-cms/Check%20&%20fix%20styling?label=code%20style)](https://github.com/blok/laravel-cms/actions?query=workflow%3A"Check+%26+fix+styling"+branch%3Amain)
[![Total Downloads](https://img.shields.io/packagist/dt/blok/laravel-cms.svg?style=flat-square)](https://packagist.org/packages/blok/laravel-cms)

A CMS Repository to connect your Headless CMS in your Laravel App.

It relies on an opinionated design pattern that we found easily in all web app project. By putting a pragmatic approach and don't reinvent the wheel you can step-up your project.

## Installation

You can install the package via composer:

```bash
composer require blok/laravel-cms
```

You can publish and run the migrations with:

```bash
php artisan vendor:publish --tag="cms-migrations"
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --tag="cms-config"
```

Then you can override and take a look at the /config/laravel-cms.php config : 



# Installation using the Strapi CMS (default one)

You must install a valid Strapi CMS from this Boilerplate and follow the instruction :

[https://github.com/uptoolkit/upcms-strapi](https://github.com/uptoolkit/upcms-strapi).

Then once you have generated a valid Token and have a valid url.

Add the config in your .env :

````dotenv
CMS_STRAPI_URL=$yourURL$
CMS_STRAPI_TOKEN=$yourToken$
````

# Installation using Wordpress CMS

You must install a valid Wordpress CMS with the needed plugins and follow the instructions here :



[https://github.com/uptoolkit/upcms-wordpress](https://github.com/uptoolkit/upcms-wordpress).

You must also install this package :

```bash
composer require cherrypulp/laravel-wordpress
```

> :warning: You must have a valid ACF Pro and WPML Pro valid licence to make it works
> :warning: You must also install [ACF to Rest Api](https://github.com/airesvsg/acf-to-rest-api) to add acf fields into your Rest API

Then once you have generated a valid Token and have a valid url.

Add the config in your .env :

````dotenv
CMS_CONNECTION=wordpress
CMS_WORDPRESS_URL=$yourURL$
CMS_WORDPRESS_TOKEN=$yourToken$
````

## Usage

Once you have setup your project. You can simple access to your CMS data through these helper methods :

```php
cms()->pageBySlug("home");
cms()->page(1);
cms()->pages($PARAMS_OF_THE_API);

/**
* Exemple in Strapi to get localized 
 * @see https://docs.strapi.io/developer-docs/latest/developer-resources/database-apis-reference/rest-api.html
 */
cms()->pages(['locale' => 'fr', 'offset' => 20, 'limit' => 10]);

/**
* Exemple in Wordpress to get localized 
 * @see https://developer.wordpress.org/rest-api/using-the-rest-api/pagination/
 * @see https://wpml.org/documentation/related-projects/woocommerce-multilingual/using-wordpress-rest-api-woocommerce-multilingual/
 */
cms()->pages(['lang' => 'fr', 'per_page' => 10, 'page' => 2]);

cms()->postBySlug("article", 'fr');
cms()->post(1);
cms()->posts($PARAMS_OF_THE_API);

cms()->emailBySlug("welcome", "en");

cms()->notificationBySlug("welcome");
cms()->settings();
cms()->setting("name");

cms()->user();
cms()->userByEmail("john@doe.com");

cms()->menus();
cms()->menu("primary_navigation");
cms()->menuBySlug("primary_navigation");


# Exemple using all the available params from Wordpress
# @see https://developer.wordpress.org/rest-api/reference/
cms()->query('posts', ['slug' => 'welcome'], ['first' => true, 'transform' => function($data, $params, $entity){
    return $data;
}, 'namespace' => 'wp/v2']);

# Exemple using all the available params from Strapi
# @see https://docs.strapi.io/developer-docs/latest/developer-resources/database-apis-reference/rest-api.html
cms()->query('posts', ['slug' => 'welcome'], ['first' => true, 'transform' => function($data, $params, $entity){
    return $data;
}]);
```

## How to access directly to the Api of the driver ?

Out of the box, the Wordpress or Strapi ServiceProvider will add a http macro [https://laravel.com/docs/9.x/http-client#macros](https://laravel.com/docs/9.x/http-client#macros) so you can request the api directly using the Http client from Laravel. 

### Access to the full Api of Strapi :

[https://docs.strapi.io/developer-docs/latest/developer-resources/database-apis-reference/rest-api.html](https://docs.strapi.io/developer-docs/latest/developer-resources/database-apis-reference/rest-api.html)

```php
Http::strapi()->get('xxxx', ['your params']);
```

### Access to the full Api of Wordpress :

[https://developer.wordpress.org/rest-api/reference/](https://developer.wordpress.org/rest-api/reference/)

```php
Http::wordpress()->get('wp/v2/xxxx', ['your params']); 
```

## How to create your custom adapter ?

If you have a custom CMS or another headless CMS used.

You can easily create your own adapter by copy-pasting one of the provider in the src/Repositories folder and overriding it inside the config folder.

At the minimum you **must define a Service Provider** and the minimum EntityRepository needed (see config for the specs.)

## How to override or handle the behavior of an entity ?

In your AppService Provider you can rebind or override the behavior of any entities in the config/laravel-cms.php.

````php
app()->bind('cms.post', function(){ return new CustomRepository();});
````

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](https://github.com/spatie/.github/blob/main/CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](../../security/policy) on how to report security vulnerabilities.

## Credits

- [Daniel Sum](https://github.com/uptoolkit)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
