<?php

return array(
    'connection' => env('CMS_CONNECTION', 'strapi'), // Default CMS_CONNECTION
    'connections' => [
        'strapi' => [
            'driver' => 'graphql',
            'provider' => Blok\Cms\Repositories\Strapi\ServiceProvider::class,
            'gql_path' => __DIR__ . '/../src/Repositories/Strapi/gql',
            'url' => env('CMS_STRAPI_URL'),
            'token' => env('CMS_STRAPI_TOKEN'),
        ],
        'wordpress' => [
            'driver' => 'rest',
            'provider' => Blok\Cms\Repositories\Wordpress\ServiceProvider::class,
            'gql_path' => __DIR__ . '/../src/Repositories/Wordpress/gql',
            'url' => env('CMS_WORDPRESS_URL'),
            'token' => env('CMS_WORDPRESS_TOKEN'),
        ],
    ],
    'entities' => [
        'custom' => [
            "article" => [
                "id" => "id",
                "attributes" => [
                    'slug',
                    'title',
                    'content',
                    'updated_at',
                    'created_at',
                ]
            ]
        ],
        'email' => [
            "id" => "id",
            "attributes" => [
                'slug',
                'title',
                'content',
                'files',
                'updated_at',
                'created_at',
            ]
        ],
        'notifications' => [
            "id" => "id",
            "attributes" => [
                'title',
                'content',
                'files',
                'updated_at',
                'created_at',
            ]
        ],
        'page' => [
            "id" => "id",
            "attributes" => [
                'title',
                'slug',
                'content',
                'updated_at',
                'created_at',
            ]
        ],
        'post' => [
            "id" => "id",
            "attributes" => [
                'title',
                'slug',
                'content',
                'updated_at',
                'created_at',
            ],
        ],
        'file' => [
            "id" => "id",
            "attributes" => [
                'alt',
                'url',
                "width",
                "height",
                "mime",
                "ext",
                'updated_at',
                'created_at',
            ]
        ],
        'setting' => [ # Be carreful this is for public data purpose !
            "id" => "id",
            "attributes" => [
                'name',
                'description',
                'url',
            ]
        ],
        'notification' => [
            "id" => "id",
            "attributes" => [
                'title',
                'slug',
                'content',
                'updated_at',
                'created_at',
            ]
        ],
        'user' => [
            "id" => "id",
            "attributes" => [
                'username',
                'email',
                'role',
                'language',
                'confirmed',
                'updated_at',
                'created_at',
            ]
        ],
        'menu' => [
            "id" => "id",
            "attributes" => [
                'name',
                'slug',
                'links',
                'updated_at',
                'created_at',
            ]
        ],
    ],
);
