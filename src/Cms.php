<?php

namespace Blok\Cms;

use Blok\Cms\Repositories\Strapi\BaseRepository;

/**
 * CMS agnostic wrapper to handle the main functions of a CMS
 */
class Cms
{
    /**
     * Return the possible correspondant
     *
     * @param $entity
     * @return BaseRepository|\Blok\Cms\Repositories\Wordpress\BaseRepository
     */
    public function getProvider($entity)
    {
        return app($entity);
    }

    /**
     * @return string
     */
    public static function getConnection(): string
    {
        return config('cms.connection');
    }

    /**
     *
     * @param $path string Endpoint of the query ex /wp/v2/pages for Wordpress
     * @param array $variables
     * @param array $options
     * @return mixed
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function query($path, array $variables = [], array $options = ['first' => false, 'transform' => true]): mixed
    {
        return $this->getProvider('cms.search')->query($path, $variables, $options);
    }

    /**
     * @param array $args
     * @return mixed
     */
    public function emails(array $args = []): mixed
    {
        return $this->getProvider('cms.email')->all($args);
    }

    /**
     * @param $id
     * @param $locale
     * @return mixed
     */
    public function email($id, $locale = null)
    {
        return $this->getProvider('cms.email')->find($id, $locale);
    }

    /**
     * @param $slug
     * @param $locale
     * @return mixed
     */
    public function emailBySlug($slug, $locale = null)
    {
        return $this->getProvider('cms.email')->findBySlug($slug, $locale);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function notifications($args = [])
    {
        return $this->getProvider('cms.notification')->all($args);
    }

    /**
     * @param $id
     * @param $locale
     * @return mixed
     */
    public function notification($id, $locale = null)
    {
        return $this->getProvider('cms.notification')->find($id, $locale);
    }

    /**
     * @param $slug
     * @param $locale
     * @return mixed
     */
    public function notificationBySlug($slug, $locale = null)
    {
        return $this->getProvider('cms.notification')->findBySlug($slug, $locale);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function pages($args = [])
    {
        return $this->getProvider('cms.page')->all($args);
    }

    /**
     * @param $id
     * @param $locale
     * @return mixed
     */
    public function page($id, $locale = null)
    {
        return $this->getProvider('cms.page')->find($id, $locale);
    }

    /**
     * @param $slug
     * @param $locale
     * @return mixed
     */
    public function pageBySlug($slug, $locale = null)
    {
        return $this->getProvider('cms.page')->findBySlug($slug, $locale);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function menus($args = [])
    {
        return $this->getProvider('cms.menu')->all($args);
    }

    /**
     * @param $id
     * @param $locale
     * @return mixed
     */
    public function menu($id, $locale = null)
    {
        return $this->getProvider('cms.menu')->find($id, $locale);
    }

    /**
     * @param $slug
     * @param $locale
     * @return mixed
     */
    public function menuBySlug($slug, $locale = null)
    {
        return $this->getProvider('cms.menu')->findBySlug($slug, $locale);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function posts($args = [])
    {
        return $this->getProvider('cms.post')->all($args);
    }

    /**
     * @param $id
     * @param $locale
     * @return mixed
     */
    public function post($id, $locale = null)
    {
        return $this->getProvider('cms.post')->find($id, $locale);
    }

    /**
     * @param $slug
     * @param $locale
     * @return mixed
     */
    public function postBySlug($slug, $locale = null)
    {
        return $this->getProvider('cms.post')->findBySlug($slug, $locale);
    }

    /**
     * @param $type
     * @return mixed
     */
    public function customPostType($type)
    {
        return $this->getProvider('cms.custom')->type($type);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function settings($args = [])
    {
        return $this->getProvider('cms.setting')->all($args);
    }

    /**
     * @param $name
     * @param $locale
     * @return mixed
     */
    public function setting($name = null, $locale = null)
    {
        return $this->getProvider('cms.setting')->get($name, $locale);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function users($args = [])
    {
        return $this->getProvider('cms.user')->all($args);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function user($id)
    {
        return $this->getProvider('cms.user')->find($id);
    }

    /**
     * @param $email
     * @return mixed
     */
    public function userByEmail($email)
    {
        return $this->getProvider('cms.user')->findByEmail($email);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function file($id)
    {
        return $this->getProvider('cms.file')->find($id);
    }

    /**
     * @param $args
     * @return mixed
     */
    public function files($args = [])
    {
        return $this->getProvider('cms.file')->all($args);
    }
}
