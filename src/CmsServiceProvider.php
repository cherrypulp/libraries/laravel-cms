<?php

namespace Blok\Cms;

use Blok\Cms\Repositories\Strapi\ServiceProvider;
use Spatie\LaravelPackageTools\Package;
use Spatie\LaravelPackageTools\PackageServiceProvider;

class CmsServiceProvider extends PackageServiceProvider
{
    public function configurePackage(Package $package): void
    {
        /*
         * This class is a Package Service Provider
         *
         * More info: https://github.com/spatie/laravel-package-tools
         */
        $package
            ->name('cms')
            ->hasConfigFile();
    }

    /**
     * @throws \Spatie\LaravelPackageTools\Exceptions\InvalidPackage
     */
    public function register()
    {
        parent::register();

        $connection = Cms::getConnection();

        // Register the provider from the config
        $serviceProvider = config('cms.connections.'.$connection.'.provider');

        /**
         * Regarding the selected connection we load the ServiceProvider from the repositories
         *
         * @var $serviceProvider ServiceProvider|\Blok\Cms\Repositories\Wordpress\ServiceProvider
         */
        $serviceProvider = new $serviceProvider();
        $serviceProvider->register();

        $this->app->singleton('cms', function(){
            return new Cms();
        });

        require_once __DIR__ . '/helpers.php';
    }

}
