<?php

namespace Blok\Cms\Contracts;

use Illuminate\Support\Collection;

interface EntityContract
{
    public function getEntity(): string;

    public function find($id, $locale = null): ?Collection;

    public function findBySlug($slug, $locale = null): ?Collection;

    public function all($args = []): ?Collection;
}
