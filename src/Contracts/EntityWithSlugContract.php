<?php

namespace Blok\Cms\Contracts;

use Illuminate\Support\Collection;

interface EntityWithSlugContract
{
    public function findBySlug($slug, $locale = null): ?Collection;
}
