<?php

namespace Blok\Cms\Contracts;

interface QueryableContract
{
    /**
     * Return the query using path
     *
     * @param $path
     * @param array $variables
     * @return array
     * @throws \ErrorException
     */
    public function query($path, array $variables = []);
}
