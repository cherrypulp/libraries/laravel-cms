<?php

namespace Blok\Cms\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class EntityNotFoundException extends ModelNotFoundException
{

}
