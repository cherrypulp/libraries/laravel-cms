<?php

namespace Blok\Cms\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Blok\Cms\Cms
 */
class Cms extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cms';
    }
}
