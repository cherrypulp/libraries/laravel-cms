<?php

namespace Blok\Cms\Repositories;

use Blok\Cms\Contracts\EntityContract;

interface CustomRepositoryInterface extends EntityContract
{
    /**
     * @param $type
     * @return string
     */
    public function type($type);
}
