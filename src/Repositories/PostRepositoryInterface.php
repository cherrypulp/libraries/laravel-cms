<?php

namespace Blok\Cms\Repositories;

use Blok\Cms\Contracts\EntityContract;
use Blok\Cms\Contracts\EntityWithSlugContract;

interface PostRepositoryInterface extends EntityContract, EntityWithSlugContract
{

}
