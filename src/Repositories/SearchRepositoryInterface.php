<?php

namespace Blok\Cms\Repositories;

use Blok\Cms\Contracts\QueryableContract;

interface SearchRepositoryInterface extends QueryableContract
{

}
