<?php

namespace Blok\Cms\Repositories;

use Blok\Cms\Contracts\EntityContract;

interface SettingRepositoryInterface extends EntityContract
{
    public function get($name, $default = null, $locale = null);
}
