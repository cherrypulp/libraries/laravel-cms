<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Contracts\EntityWithSlugContract;
use Blok\Cms\Contracts\QueryableContract;
use Blok\Cms\Contracts\EntityContract;
use Blok\Cms\Exceptions\EntityNotFoundException;
use Blok\Utils\Arr;
use Http;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

abstract class BaseRepository implements QueryableContract, EntityContract, EntityWithSlugContract
{
    /**
     * @param $args
     * @return Collection|null
     * @throws RequestException
     */
    public function all($args = []): ?Collection
    {
        return $this->query(ucfirst($this->getEntity()) . 'Query', $args);
    }

    /**
     * @return PendingRequest
     */
    public function graphql(): PendingRequest
    {
        return Http::strapi();
    }

    /**
     * @return PendingRequest
     */
    public function http(): PendingRequest
    {
        return Http::strapi();
    }

    /**
     * Return the path where gql sit
     * @param null $path
     * @return string
     */
    public function getPath($path = null): string
    {
        if ($path && !is_file($path)) {

            if (!Str::endsWith($path, '.gql')) {
                $path .= ".gql";
            }

            $path = config("cms.connections.strapi.gql_path") . DIRECTORY_SEPARATOR . $path;
        } else {
            $path = config("cms.connections.strapi.gql_path") . DIRECTORY_SEPARATOR;
        }

        return $path;
    }

    /**
     * Return the gql from the selected folder and inject Fragment if posted
     *
     * @param $gql
     * @return false|string
     * @throws \Exception
     */
    public function getGql($path): false|string
    {
        // If not the full path we generate the path
        if (!is_file($path)) {
            if (!Str::endsWith($path, '.gql')) {
                $path .= ".gql";
            }
            $path = config("cms.connections.strapi.gql_path") . DIRECTORY_SEPARATOR . $path;
        }

        if (!is_file($path)) {
            throw new \Exception('Invalid path ' . $path);
        }

        $dir = \File::dirname($path);

        $gql = file_get_contents($path);

        $fragments = [];

        return $this->addFragmentToGql($gql, $fragments, $dir);
    }

    protected function addFragmentToGql($gql, &$fragments, $dir):string
    {
        preg_match_all("/\.\.\.(.*)Fragment/i", $gql, $matches);

        if ($matches[1]) {
            foreach ($matches[1] as $fragment) {
                if (!in_array($fragment, $fragments, true)) {
                    $fragments[] = $fragment;
                    if ($fragment = file_get_contents($dir . DIRECTORY_SEPARATOR . $fragment . 'Fragment.gql')) {
                        $gql .= $fragment;
                        $gql = $this->addFragmentToGql($gql, $fragments, $dir);
                    }
                }
            }
        }

        return $gql;
    }

    /**
     * Return the query using path
     *
     * @param $path
     * @param array $variables
     * @return Collection
     * @throws RequestException
     */
    public function query($path, array $variables = [], $params = ['first' => false, 'transform' => true]):Collection
    {
        Arr::mergeWithDefaultParams($params);

        $variables = array_merge(['locale' => app()->getLocale()], $variables);

        $response = $this->graphql()->post('graphql', [
            'query' => $this->getGql($path), 'variables' => $variables]);

        if ($response->successful()) {
            $data = $response->object();

            return $this->applyTransform($data, $params);
        }

        throw $response->throw();
    }

    /**
     * @param $data
     * @param $params
     * @return array|mixed
     * @throws EntityNotFoundException
     */
    public function applyTransform($data, $params): mixed
    {
        if ($params['transform']) {
            if ($params['transform'] instanceof \Closure) {
                return $params['transform']($data, $params, $this->getEntity());
            }

            if ($params['first']) {
                return $this->mergeAttributes(collect($data->data->{Str::plural($this->getEntity())}->data[0] ?? null));
            }

            return collect($data->data->{Str::plural($this->getEntity())}->data)->map(fn ($data) => $this->mergeAttributes($data));
        }

        return $data;
    }

    /**
     * @param Collection $data
     * @return Collection
     */
    public function mergeAttributes(Collection $data): Collection
    {
        return $data->union($data->get('attributes'))->forget('attributes');
    }

    /**
     * @param $id
     * @param null $locale
     * @return Collection|null
     * @throws RequestException
     */
    public function find($id, $locale = null): ?Collection
    {
        return $this->query(ucfirst($this->getEntity()) . 'Query', ['id' => $id], ['first' => true]);
    }

    /**
     * @param $slug
     * @param null $locale
     * @return Collection|null
     * @throws RequestException
     */
    public function findBySlug($slug, $locale = null): ?Collection
    {
        return $this->query(ucfirst($this->getEntity()) . 'BySlugQuery', ['slug' => $slug], ['first' => true]);
    }

    /**
     * Retrieve the entity name
     *
     * @return string
     */
    public function getEntity(): string
    {
        $class = str_replace("Repository", "", class_basename(static::class));
        return strtolower($class);
    }
}
