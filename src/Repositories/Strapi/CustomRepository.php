<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Repositories\CustomRepositoryInterface;

class CustomRepository extends BaseRepository implements CustomRepositoryInterface
{
    protected string $type;

    /**
     * @param $type
     * @return static
     */
    public function type($type): static
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @throws \ErrorException
     */
    public function find($id, $locale = null):array
    {
        $path = $this->getPath(ucfirst($this->getType()).'Query');

        return $this->query($path, ['id' => $id]);
    }

    /**
     * @throws \ErrorException
     */
    public function findBySlug($slug, $locale = null):array
    {
        $path = $this->getPath(ucfirst($this->getType()).'BySlugQuery');

        return $this->query($path, ['slug' => $slug]);
    }

    /**
     * @throws \Exception
     */
    public function getType(){
        if (!$this->type) {
            throw new \Exception('You must define a type using ->type() before using it');
        }
        return $this->type;
    }

    public function getEntity(): string
    {
        return $this->type;
    }

    public function all($args = []): ?\Illuminate\Support\Collection
    {
        return [];
    }
}
