<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Repositories\FileRepositoryInterface;

class FileRepository extends BaseRepository implements FileRepositoryInterface
{
    /**
     * @param $id
     * @return array
     * @throws \ErrorException
     */
    public function find($id, $locale = null): array
    {
        return $this->query('FileQuery');
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return "uploadFiles";
    }

    public function all($args = []): ?\Illuminate\Support\Collection
    {
        return [];
    }
}
