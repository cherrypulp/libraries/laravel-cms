<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Repositories\MenuRepositoryInterface;

class MenuRepository extends BaseRepository implements MenuRepositoryInterface
{
    public function getEntity(): string
    {
        return "menu";
    }
}
