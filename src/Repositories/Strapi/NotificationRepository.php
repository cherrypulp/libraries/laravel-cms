<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Repositories\NotificationRepositoryInterface;

class NotificationRepository extends BaseRepository implements NotificationRepositoryInterface
{
    public function getEntity(): string
    {
        return "notification";
    }
}
