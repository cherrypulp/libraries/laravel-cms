<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Repositories\PageRepositoryInterface;

class PageRepository extends BaseRepository implements PageRepositoryInterface
{
    public function getEntity(): string
    {
        return "page";
    }
}
