<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Repositories\PageRepositoryInterface;

class SearchRepository extends BaseRepository implements PageRepositoryInterface
{
    public function getEntity(): string
    {
        return "post";
    }
}
