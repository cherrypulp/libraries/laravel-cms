<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Cms;
use Blok\Graphql\Graphql;
use Http;

class ServiceProvider
{
    /**
     * Register the Strapi Service Provider
     *
     * @return void
     */
    public function register(): void
    {
        $entities = config('cms.entities');

        foreach ($entities as $entity => $attr) {

            $class = "\\Blok\\Cms\\Repositories\\Strapi\\" . ucfirst($entity) . 'Repository';

            app()->bind('cms.' . $entity, function () use ($class, $attr) {
                return new $class($attr);
            });
        }

        app()->bind('cms.search', function(){
            return new SearchRepository();
        });

        Http::macro('strapi', function () {
            return Http::withHeaders([
                'Authorization' => 'Bearer '.config('cms.connections.strapi.token'), #Token generated in the admin
            ])->baseUrl(config('cms.connections.strapi.url')); # Base url of your strapi app
        });

        app()->singleton(Cms::class . '.graphql', function () {
            return new Graphql(config('cms.connections.strapi.url').'/graphql', config('cms.connections.strapi.token'));
        });
    }
}
