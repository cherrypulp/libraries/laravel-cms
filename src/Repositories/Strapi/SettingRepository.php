<?php

namespace Blok\Cms\Repositories\Strapi;

use Blok\Cms\Repositories\SettingRepositoryInterface;
use Illuminate\Support\Collection;

class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{
    protected $items = null;

    public function getEntity(): string
    {
        return "setting";
    }

    public function all($args = []): ?Collection
    {
        if (!$this->items) {
            $res = $this->http()->get('api/setting', $args);

            if ($res->successful()) {
                $data = $res->json('data');
                $this->items = $data['attributes'];
            } else{
                $res->throw();
            }
        }

        return collect($this->items);
    }


    /**
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function get($name, $default = null, $locale = null)
    {
        $data = $this->all(['locale' => $locale ?? app()->getLocale()]);

        return $data[$name] ?? $default;
    }
}
