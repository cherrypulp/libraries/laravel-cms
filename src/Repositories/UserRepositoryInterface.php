<?php

namespace Blok\Cms\Repositories;

interface UserRepositoryInterface
{
    public function all($args = []);
    public function find($id);
    public function findByEmail($email);
}
