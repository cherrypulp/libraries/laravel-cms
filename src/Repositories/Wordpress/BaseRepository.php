<?php

namespace Blok\Cms\Repositories\Wordpress;

use Blok\Cms\Contracts\EntityWithSlugContract;
use Blok\Cms\Contracts\QueryableContract;
use Blok\Cms\Contracts\EntityContract;
use Blok\Cms\Exceptions\EntityNotFoundException;
use Blok\Utils\Arr;
use Http;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Wordpress Base Repository
 */
abstract class BaseRepository implements QueryableContract, EntityContract, EntityWithSlugContract
{
    /**
     * @param $args
     * @return Collection|null
     * @throws RequestException
     */
    public function all($args = []): ?Collection
    {
        return $this->query($this->getEntityInPlural(), $args, ['first' => false]);
    }

    /**
     * @return \Blok\Graphql\Graphql
     */
    public function graphql()
    {
        return lwp()->graphql();
    }

    /**
     * @return PendingRequest|Http
     */
    public function http(): PendingRequest|Http
    {
        return lwp()->http();
    }

    /**
     * Return the path where gql sit
     * @param null $path
     * @return string
     */
    public function getPath($path = null): string
    {
        if ($path && !is_file($path)) {

            if (!Str::endsWith($path, '.gql')) {
                $path .= ".gql";
            }

            $path = config("cms.connections.wordpress.gql_path") . DIRECTORY_SEPARATOR . $path;
        } else {
            $path = config("cms.connections.wordpress.gql_path") . DIRECTORY_SEPARATOR;
        }

        return $path;
    }

    /**
     * Return the gql from the selected folder and inject Fragment if posted
     *
     * @param $gql
     * @return false|string
     * @throws \Exception
     */
    public function getGql($path): false|string
    {
        // If not the full path we generate the path
        if (!is_file($path)) {
            if (!Str::endsWith($path, '.gql')) {
                $path .= ".gql";
            }
            $path = config("cms.connections.wordpress.gql_path") . DIRECTORY_SEPARATOR . $path;
        }

        if (!is_file($path)) {
            throw new \Exception('Invalid path ' . $path);
        }

        $dir = \File::dirname($path);

        $gql = file_get_contents($path);

        $fragments = [];

        return $this->addFragmentToGql($gql, $fragments, $dir);
    }

    protected function addFragmentToGql($gql, &$fragments, $dir): string
    {
        preg_match_all("/\.\.\.(.*)Fragment/i", $gql, $matches);

        if ($matches[1]) {
            foreach ($matches[1] as $fragment) {
                if (!in_array($fragment, $fragments, true)) {
                    $fragments[] = $fragment;
                    if ($fragment = file_get_contents($dir . DIRECTORY_SEPARATOR . $fragment . 'Fragment.gql')) {
                        $gql .= $fragment;
                        $gql = $this->addFragmentToGql($gql, $fragments, $dir);
                    }
                }
            }
        }

        return $gql;
    }

    /**
     * Return the query using path
     *
     * @param $path string Path to the corresponding entity default /wp/v2/xxx (for your endpoint namespace use options namespace)
     * @param array $params Params used in the query wp-json
     * @param array $options
     * @return Collection
     * @throws RequestException
     * @throws \Exception
     */
    public function query($path, array $params = [], array $options = ['first' => false, 'transform' => true, 'namespace' => 'wp/v2/']): Collection|null|string
    {
        $options = array_merge(['first' => false, 'transform' => true, 'namespace' => 'wp/v2/'], $options);

        $params = array_merge(['lang' => app()->getLocale(), '_embed' => 1], $params);

        // If no embed => remove embed from default params
        if (isset($options['noembed']) && $options['noembed']) {
            unset($params['_embed']);
        }

        $endpoint = \Blok\Utils\Str::mustEndWith('/', $options['namespace']) . $path;

        if (isset($params['id'])) {
            $endpoint .= "/" . $params['id'];
        }

        $response = $this->http()->get($endpoint, $params);

        if (config('app.debug')) {
            \Log::info("Api call", ['endpoint' => $endpoint, 'params' => $params]);
        }

        if ($response->successful()) {

            $data = $response->object();

            if (isset($params['id'])) {
                $options['first'] = false;
            }

            return $this->applyTransform($data, $options);
        }

        \Log::error("Api call error", ['endpoint' => $endpoint, 'params' => $params]);

        throw $response->throw();
    }

    /**
     * @param $data
     * @param $params
     * @return array|mixed
     * @throws EntityNotFoundException
     */
    public function applyTransform($data, $params): mixed
    {
        if ($params['transform']) {
            if ($params['transform'] instanceof \Closure) {
                return $params['transform']($data, $params, $this->getEntity());
            }

            if ($params['first']) {

                if (!isset($data[0])) {
                    if (@$params['throw_exceptions']) {
                        throw new EntityNotFoundException();
                    } else {
                        return null;
                    }
                }

                return collect($data[0]);
            }

            return collect($data);
        }

        return $data;
    }

    /**
     * @param Collection $data
     * @return Collection
     */
    public function mergeAttributes(Collection $data): Collection
    {
        return $data->union($data->get('attributes'))->forget('attributes');
    }

    /**
     * @param $id
     * @param null $locale
     * @return Collection|null
     * @throws RequestException
     */
    public function find($id, $locale = null): ?Collection
    {
        return $this->query($this->getEntityInPlural(), ['id' => $id, 'lang' => $locale], ['first' => true]);
    }

    /**
     * @param $slug
     * @param null $locale
     * @return Collection|null
     * @throws RequestException
     */
    public function findBySlug($slug, $locale = null): ?Collection
    {
        return $this->query($this->getEntityInPlural(), ['slug' => $slug, 'lang' => $locale], ['first' => true, 'namespace' => 'wp/v2/']);
    }

    /**
     * Retrieve the entity name
     *
     * @return string
     */
    public function getEntity(): string
    {
        $class = str_replace("Repository", "", class_basename(static::class));
        return strtolower($class);
    }

    /**
     * Get plural form
     *
     * @return string
     */
    public function getEntityInPlural(): string
    {
        return Str::plural($this->getEntity());
    }
}
