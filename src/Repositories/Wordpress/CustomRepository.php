<?php

namespace Blok\Cms\Repositories\Wordpress;

use Blok\Cms\Repositories\CustomRepositoryInterface;

class CustomRepository extends BaseRepository implements CustomRepositoryInterface
{
    protected string $type;

    /**
     * @param $type
     * @return static
     */
    public function type($type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getEntity(): string
    {
        if (!$this->type) {
            throw new \Exception('You must define a type first !');
        }

        return $this->type;
    }
}
