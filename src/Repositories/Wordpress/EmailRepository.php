<?php

namespace Blok\Cms\Repositories\Wordpress;

use Blok\Cms\Repositories\EmailRepositoryInterface;

class EmailRepository extends BaseRepository implements EmailRepositoryInterface
{
    /**
     * Return current entity name
     *
     * @return string
     */
    public function getEntity(): string
    {
        return "email";
    }
}
