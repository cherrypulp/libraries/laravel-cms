<?php

namespace Blok\Cms\Repositories\Wordpress;

use Blok\Cms\Repositories\FileRepositoryInterface;

class FileRepository extends BaseRepository implements FileRepositoryInterface
{
    /**
     * @return string
     */
    public function getEntity(): string
    {
        return "media";
    }
}
