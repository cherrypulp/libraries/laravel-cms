<?php

namespace Blok\Cms\Repositories\Wordpress;

use Blok\Cms\Repositories\MenuRepositoryInterface;
use Illuminate\Support\Collection;

class MenuRepository extends BaseRepository implements MenuRepositoryInterface
{
    public function getEntity(): string
    {
        return "menu";
    }

    public function findBySlug($slug, $locale = null): ?Collection
    {
        return $this->query($this->getEntityInPlural(), ['id' => $slug, 'lang' => $locale], ['first' => true, 'namespace' => 'menus/v1/', 'noembed' => true]);
    }

    public function find($slug, $locale = null): ?Collection
    {
        return $this->query($this->getEntityInPlural(), ['id' => $slug, 'lang' => $locale], ['first' => true, 'namespace' => 'menus/v1/', 'noembed' => true]);
    }

    public function all($args = []): ?Collection
    {
        return $this->query($this->getEntityInPlural(), $args, ['first' => false, 'namespace' => 'menus/v1/', 'noembed' => true]);
    }
}
