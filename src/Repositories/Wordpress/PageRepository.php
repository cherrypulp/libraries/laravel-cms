<?php

namespace Blok\Cms\Repositories\Wordpress;

use Blok\Cms\Repositories\PageRepositoryInterface;
use Illuminate\Support\Collection;

class PageRepository extends BaseRepository implements PageRepositoryInterface
{
    public function getEntity(): string
    {
        return "page";
    }
}
