<?php

namespace Blok\Cms\Repositories\Wordpress;

use App\Models\User;
use Blok\Cms\Cms;
use Blok\Graphql\Graphql;
use Http;

class ServiceProvider
{
    /**
     * Register the Wordpress Service Provider
     *
     * @return void
     */
    public function register(): void
    {
        $entities = config('cms.entities');

        /**
         * Bind entities to app so we will be able to override it separately
         */
        foreach ($entities as $entity => $attr) {

            $class = "\\Blok\\Cms\\Repositories\\Wordpress\\" . ucfirst($entity) . 'Repository';

            app()->bind('cms.' . $entity, function () use ($class, $attr) {
                return new $class($attr);
            });
        }

        // Bind the query search
        app()->bind('cms.search', function(){
            return new SearchRepository();
        });

        /**
         * Add an http macro as standalone
         */
        Http::macro('wordpress', function () {
            return Http::withHeaders([
                'Authorization' => 'Bearer '.config('cms.connections.wordpress.token'), #Token generated in the admin
            ])->baseUrl(config('cms.connections.wordpress.url')); # Base url of your wordpress app
        });

        app()->singleton(Cms::class . '.graphql', function () {
            return lwp()->graphql();
        });
    }
}
