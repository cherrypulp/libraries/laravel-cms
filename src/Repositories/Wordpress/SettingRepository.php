<?php

namespace Blok\Cms\Repositories\Wordpress;

use Blok\Cms\Repositories\SettingRepositoryInterface;
use Google\Collection;

class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{
    protected $items;

    public function getEntity(): string
    {
        return "setting";
    }

    /**
     * @param array $args
     * @return \Illuminate\Support\Collection|null
     * @throws \Illuminate\Http\Client\RequestException
     */
    public function all($args = []): \Illuminate\Support\Collection|null
    {
        if (!$this->items) {
            $res = $this->http()->get('acf/v3/options/options/');

            if ($res->successful()) {
                $data = $res->json('acf');
                $this->items = $data;
            } else{
                $res->throw();
            }
        }

        return collect($this->items);
    }

    public function get($name, $default = null, $locale = null)
    {
        $data = $this->all();

        return $data[$name] ?? $default;
    }
}
