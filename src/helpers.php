<?php

if(!function_exists("cms")){
    /**
     * Helpers to get the Cms available easily with autocomplete
     *
     * @return \Blok\Cms\Cms
     */
    function cms()
    {
        return app("cms");
    }
}
