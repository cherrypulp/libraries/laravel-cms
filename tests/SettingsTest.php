<?php

it('can access to settings', function () {
    expect(cms()->settings())->toHaveCount();
});

it('can query posts and return only their title', function () {
    expect(cms()->query('posts', ['_fields' => 'title']))->toHaveCount();
});
